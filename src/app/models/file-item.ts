export class FileItem {
  public archivo: File;
  public nombreArchivo: string;
  public url: string;
  public id: string;
  public tags: string;
  public estaSubiendo: boolean;
  public progreso : number;



  constructor ( archivo: File, tags: string ) {

    this.archivo = archivo;
    this.nombreArchivo = archivo.name;
    this.tags = tags;

    this.estaSubiendo = false;
    this.progreso = 0;

  }

 }
