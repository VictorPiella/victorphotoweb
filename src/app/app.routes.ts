import {Routes, RouterModule}  from '@angular/router';
import { PhotosComponent } from './components/photos/photos.component';
import { CargaComponent } from './components/carga/carga.component';

const RUTAS: Routes = [
  { path: 'photos', component: PhotosComponent},
  { path: 'carga', component: CargaComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'photos'},
];

export const APP_ROUTES = RouterModule.forRoot( RUTAS );
